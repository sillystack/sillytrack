import '../models/manifest.dart';
import '../models/summary.dart';

class Waybill {
  bool delivered;
  Summary summary;
  List<Manifest> manifest;
  String noresi;
  String expedisi;

  Waybill();

  Waybill.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      delivered = jsonMap['delivered'] ?? false;
      noresi = jsonMap['no_resi'] != null ? jsonMap['no_resi'] : '';
      expedisi = jsonMap['expedisi'] != null ? jsonMap['expedisi'] : '';
      summary = jsonMap['summary'] != null
          ? Summary.fromJSON(jsonMap['summary'])
          : Summary.fromJSON({});
      manifest = jsonMap['manifest'] != null
          ? List.from(jsonMap['manifest'])
              .map((element) => Manifest.fromJSON(element))
              .toList()
          : [];
    } catch (e) {
      delivered = false;
      noresi = '';
      expedisi = '';
      summary = Summary.fromJSON({});
      manifest = [];
    }
  }
}
