class Summary {
  String courierCode;
  String courierName;
  String waybillNumber;
  String serviceCode;
  String waybillDate;
  String shipperName;
  String receiverName;
  String origin;
  String destination;
  String status;

  Summary();

  Summary.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      courierCode =
          jsonMap['courier_code'] != null ? jsonMap['courier_code'] : '';
      courierName =
          jsonMap['courier_name'] != null ? jsonMap['courier_name'] : '';
      waybillNumber =
          jsonMap['waybill_number'] != null ? jsonMap['waybill_number'] : '';
      serviceCode =
          jsonMap['service_code'] != null ? jsonMap['service_code'] : '';
      waybillDate =
          jsonMap['waybill_date'] != null ? jsonMap['waybill_date'] : '';
      shipperName =
          jsonMap['shipper_name'] != null ? jsonMap['shipper_name'] : '';
      receiverName =
          jsonMap['receiver_name'] != null ? jsonMap['receiver_name'] : '';
      origin = jsonMap['origin'] != null ? jsonMap['origin'] : '';
      destination =
          jsonMap['destination'] != null ? jsonMap['destination'] : '';
      status = jsonMap['status'] != null ? jsonMap['status'] : '';
    } catch (e) {
      courierCode = '';
      courierName = '';
      waybillNumber = '';
      waybillDate = '';
      serviceCode = '';
      shipperName = '';
      receiverName = '';
      origin = '';
      destination = '';
      status = '';
    }
  }
}
