class Manifest {
  String manifestCode;
  String manifestName;
  String manifestDate;
  String manifestTime;
  String cityName;

  Manifest();

  Manifest.fromJSON(Map<String, dynamic> jsonMap) {
    try {
      manifestCode =
          jsonMap['manifest_code'] != null ? jsonMap['manifest_code'] : '';
      manifestName = jsonMap['manifest_description'] != null
          ? jsonMap['manifest_description']
          : '';
      manifestDate =
          jsonMap['manifest_date'] != null ? jsonMap['manifest_date'] : '';
      manifestTime =
          jsonMap['manifest_time'] != null ? jsonMap['manifest_time'] : '';
      cityName = jsonMap['city_name'] != null ? jsonMap['city_name'] : '';
    } catch (e) {
      manifestCode = '';
      manifestName = '';
      manifestDate = '';
      manifestTime = '';
      cityName = '';
    }
  }
}
