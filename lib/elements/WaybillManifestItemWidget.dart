import 'package:flutter/material.dart';
import 'package:sillytrack/models/waybill.dart';
import '../models/manifest.dart';

// ignore: must_be_immutable
class WaybillManifestItemWidget extends StatelessWidget {
  String heroTag;
  Manifest manifest;

  WaybillManifestItemWidget({Key key, this.heroTag, this.manifest})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
        decoration: BoxDecoration(
          color: Theme.of(context).highlightColor.withOpacity(0.9),
          boxShadow: [
            BoxShadow(
                color: Theme.of(context).focusColor.withOpacity(0.0),
                blurRadius: 5,
                offset: Offset(0, 2)),
          ],
        ),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                child: Text(
                  manifest.manifestDate + ' ' + manifest.manifestTime,
                  textAlign: TextAlign.left,
                ),
              ),
              Container(
                child: Text(
                  manifest.manifestName,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.left,
                ),
              ),
            ]));
  }
}
