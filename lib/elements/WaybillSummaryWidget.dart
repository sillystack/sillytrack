import 'package:flutter/material.dart';
import 'package:sillytrack/models/waybill.dart';

// ignore: must_be_immutable
class WaybillSummaryWidget extends StatelessWidget {
  final Waybill waybill;

  WaybillSummaryWidget({Key key, this.waybill}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        decoration: BoxDecoration(
          color: Theme.of(context).highlightColor,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
                color: Theme.of(context).hintColor.withOpacity(0.1),
                offset: Offset(0, 3),
                blurRadius: 5)
          ],
        ),
        child: ListView(shrinkWrap: true, primary: false, children: <Widget>[
          ListTile(
            title: Text(
              'No Resi',
              style: Theme.of(context).textTheme.bodyText1,
            ),
            trailing: Text(
              waybill.summary.waybillNumber,
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
          ListTile(
            title: Text(
              'Status',
              style: Theme.of(context).textTheme.bodyText1,
            ),
            trailing: Text(
              waybill.summary.status,
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
          ListTile(
            title: Text(
              'Service',
              style: Theme.of(context).textTheme.bodyText1,
            ),
            trailing: Text(
              waybill.summary.serviceCode,
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
          ListTile(
            title: Text(
              'Di kirim Tanggal',
              style: Theme.of(context).textTheme.bodyText1,
            ),
            trailing: Text(
              waybill.summary.waybillDate,
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
          ListTile(
            title: Text(
              'Dikirim ke',
              style: Theme.of(context).textTheme.bodyText1,
            ),
            trailing: Text(
              waybill.summary.receiverName,
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
        ]));
  }
}
