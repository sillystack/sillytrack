import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class BillWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  BillWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _BillWidgetState createState() => _BillWidgetState();
}

class _BillWidgetState extends StateMVC<BillWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          title: Text("Ongkir"),
        ),
        body: SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Text("Ongkos Kirim"),
                  )
                ])));
  }
}
