import 'package:flutter/material.dart';

import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:sillytrack/elements/WaybillManifestItemWidget.dart';
import 'package:sillytrack/elements/WaybillSummaryWidget.dart';
import '../elements/BlockButtonWidget.dart';
import '../controllers/waybill_controller.dart';

class ResiWidget extends StatefulWidget {
  @override
  _ResiWidgetState createState() => _ResiWidgetState();
}

class _ResiWidgetState extends StateMVC<ResiWidget> {
  WaybillController _con;
  List<String> _expedisi = [
    'JNE',
    'TIKI',
    'JNT',
    'POS',
    'LION',
    'NINJA',
    'SICEPAT',
    'IDE',
    'SAP',
    'NCS',
    'ANTERAJA',
    'REX',
    'SENTRAL',
    'RPX',
    'PANDU',
    'WAHANA',
    'PAHALA',
    'JET',
    'SLIS',
    'EXPEDITO',
    'DSE',
    'FIRST',
    'STAR',
    'IDL'
  ];

  _ResiWidgetState() : super(WaybillController()) {
    _con = controller;
  }

  InputDecoration getInputDecoration({String hintText, String labelText}) {
    return new InputDecoration(
      hintText: hintText,
      labelText: labelText,
      hintStyle: Theme.of(context).textTheme.bodyText2.merge(
            TextStyle(color: Theme.of(context).focusColor),
          ),
      enabledBorder: UnderlineInputBorder(
          borderSide:
              BorderSide(color: Theme.of(context).hintColor.withOpacity(0.2))),
      focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Theme.of(context).hintColor)),
      floatingLabelBehavior: FloatingLabelBehavior.auto,
      labelStyle: Theme.of(context).textTheme.bodyText2.merge(
            TextStyle(color: Theme.of(context).hintColor),
          ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _con.scaffoldKey,
        body: RefreshIndicator(
            onRefresh: _con.refreshPage,
            child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(vertical: 50),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            color: Theme.of(context).highlightColor,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                blurRadius: 50,
                                color: Theme.of(context)
                                    .hintColor
                                    .withOpacity(0.2),
                              )
                            ]),
                        margin: EdgeInsets.symmetric(
                          horizontal: 20,
                        ),
                        padding: EdgeInsets.only(
                            top: 50, right: 27, left: 27, bottom: 20),
                        child: Form(
                          key: _con.loginFormKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              TextFormField(
                                keyboardType: TextInputType.text,
                                onSaved: (input) => _con.waybill.noresi = input,
                                validator: (input) => input.trim().length < 3
                                    ? 'No Resi Salah'
                                    : null,
                                decoration: InputDecoration(
                                  labelText: 'No Resi',
                                  labelStyle: TextStyle(
                                      color: Theme.of(context).accentColor),
                                  contentPadding: EdgeInsets.all(12),
                                  hintText: 'Input no resi',
                                  hintStyle: TextStyle(
                                      color: Theme.of(context)
                                          .focusColor
                                          .withOpacity(0.7)),
                                  prefixIcon: Icon(Icons.receipt_long,
                                      color: Theme.of(context).accentColor),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.2))),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.5))),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.2))),
                                ),
                              ),
                              new DropdownButtonFormField(
                                style: TextStyle(
                                    color: Theme.of(context).hintColor),
                                items: _expedisi.map((String expedisi) {
                                  return new DropdownMenuItem(
                                      value: expedisi,
                                      child: Row(
                                        children: <Widget>[
                                          Icon(Icons.arrow_right),
                                          Text(expedisi),
                                        ],
                                      ));
                                }).toList(),
                                onChanged: (input) =>
                                    _con.waybill.expedisi = input,
                                decoration: InputDecoration(
                                    hintText: 'Pilih Jasa Ekspedisi'),
                                validator: (input) => input == null
                                    ? 'Jasa Ekspedisi harap di isi'
                                    : null,
                              ),
                              SizedBox(height: 30),
                              BlockButtonWidget(
                                text: Text(
                                  'Cari',
                                  style: Theme.of(context).textTheme.button,
                                ),
                                color: Theme.of(context).accentColor,
                                onPressed: () {
                                  _con.checkResi();
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      _con.waybill.delivered == true
                          ? WaybillSummaryWidget(waybill: _con.waybill)
                          : SizedBox(
                              height: 6,
                            ),
                      _con.waybill.delivered == true
                          ? Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 15),
                              decoration: BoxDecoration(
                                color: Theme.of(context).highlightColor,
                                borderRadius: BorderRadius.circular(5),
                                boxShadow: [
                                  BoxShadow(
                                      color: Theme.of(context)
                                          .hintColor
                                          .withOpacity(0.1),
                                      offset: Offset(0, 3),
                                      blurRadius: 5)
                                ],
                              ),
                              child: ListView.separated(
                                padding: EdgeInsets.only(
                                    bottom: 100, top: 15, left: 15, right: 15),
                                physics: const ClampingScrollPhysics(),
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                itemCount: _con.waybill.manifest.length,
                                separatorBuilder: (context, index) {
                                  return SizedBox(height: 15);
                                },
                                itemBuilder: (context, index) {
                                  return WaybillManifestItemWidget(
                                    manifest:
                                        _con.waybill.manifest.elementAt(index),
                                    heroTag: 'manifest',
                                  );
                                },
                              ))
                          : SizedBox(
                              height: 5,
                            )
                    ]))));
  }
}
