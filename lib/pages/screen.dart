import 'package:flutter/material.dart';

import 'package:mvc_pattern/mvc_pattern.dart';

class ScreenWidget extends StatefulWidget {
  @override
  _ScreenWidgetState createState() => _ScreenWidgetState();
}

class _ScreenWidgetState extends StateMVC<ScreenWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(
          vertical: 20,
        ),
        child: Text(
          'Comming Soon',
          textAlign: TextAlign.center,
        ));
  }
}
