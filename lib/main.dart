import 'dart:io';

import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:sillytrack/models/setting.dart';
import 'package:sillytrack/route_generator.dart';
import './repository/settings_repository.dart' as repo;

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GlobalConfiguration().loadFromAsset("configurations");
  print("base_url: ${GlobalConfiguration().getString('base_url')}");
  HttpOverrides.global = new MyHttpOverrides();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
//  /// Supply 'the Controller' for this application.
//  MyApp({Key key}) : super(con: Controller(), key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    repo.initSettings();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: repo.setting,
        builder: (context, Setting _setting, _) {
          return MaterialApp(
              navigatorKey: repo.navigatorKey,
              title: _setting.appName,
              initialRoute: '/Splash',
              onGenerateRoute: RouteGenerator.generateRoute,
              debugShowCheckedModeBanner: false,
              // locale: _setting.mobileLanguage.value,
              // localizationsDelegates: [
              //   S.delegate,
              //   GlobalMaterialLocalizations.delegate,
              //   GlobalWidgetsLocalizations.delegate,
              //   GlobalCupertinoLocalizations.delegate,
              // ],
              // supportedLocales: S.delegate.supportedLocales,
              theme: _setting.brightness.value == Brightness.light
                  ? ThemeData(
                      fontFamily: 'ProductSans',
                      primaryColor: Colors.white,
                      floatingActionButtonTheme: FloatingActionButtonThemeData(
                          elevation: 0, foregroundColor: Colors.white),
                      brightness: Brightness.light,
                      // accentColor: config.Colors().mainColor(1),
                      // dividerColor: config.Colors().accentColor(0.1),
                      // focusColor: config.Colors().accentColor(1),
                      // hintColor: config.Colors().secondColor(1),
                      // textTheme: TextTheme(
                      //   headline5: TextStyle(
                      //       fontSize: 20.0,
                      //       color: config.Colors().secondColor(1),
                      //       height: 1.3),
                      //   headline4: TextStyle(
                      //       fontSize: 18.0,
                      //       fontWeight: FontWeight.w300,
                      //       color: config.Colors().secondColor(1),
                      //       height: 1.3),
                      //   headline3: TextStyle(
                      //       fontSize: 20.0,
                      //       fontWeight: FontWeight.w300,
                      //       color: config.Colors().secondColor(1),
                      //       height: 1.3),
                      //   headline2: TextStyle(
                      //       fontSize: 22.0,
                      //       fontWeight: FontWeight.w300,
                      //       color: config.Colors().mainColor(1),
                      //       height: 1.4),
                      //   headline1: TextStyle(
                      //       fontSize: 24.0,
                      //       fontWeight: FontWeight.w300,
                      //       color: config.Colors().secondColor(1),
                      //       height: 1.4),
                      //   subtitle1: TextStyle(
                      //       fontSize: 16.0,
                      //       fontWeight: FontWeight.w500,
                      //       color: config.Colors().secondColor(1),
                      //       height: 1.3),
                      //   headline6: TextStyle(
                      //       fontSize: 15.0,
                      //       fontWeight: FontWeight.w300,
                      //       color: config.Colors().mainColor(1),
                      //       height: 1.3),
                      //   bodyText2: TextStyle(
                      //       fontSize: 12.0,
                      //       fontWeight: FontWeight.w300,
                      //       color: config.Colors().secondColor(1),
                      //       height: 1.2),
                      //   bodyText1: TextStyle(
                      //       fontSize: 13.0,
                      //       fontWeight: FontWeight.w300,
                      //       color: config.Colors().secondColor(1),
                      //       height: 1.3),
                      //   caption: TextStyle(
                      //       fontSize: 12.0,
                      //       fontWeight: FontWeight.w300,
                      //       color: config.Colors().accentColor(1),
                      //       height: 1.2),
                      // ),
                    )
                  : ThemeData(
                      fontFamily: 'ProductSans',
                      primaryColor: Color(0xFF252525),
                      brightness: Brightness.dark,
                      scaffoldBackgroundColor: Color(0xFF2C2C2C),
                      // accentColor: config.Colors().mainDarkColor(1),
                      // dividerColor: config.Colors().accentColor(0.1),
                      // hintColor: config.Colors().secondDarkColor(1),
                      // focusColor: config.Colors().accentDarkColor(1),
                      // textTheme: TextTheme(
                      //   headline5: TextStyle(
                      //       fontSize: 22.0,
                      //       color: config.Colors().secondDarkColor(1),
                      //       height: 1.3),
                      //   headline4: TextStyle(
                      //       fontSize: 20.0,
                      //       fontWeight: FontWeight.w700,
                      //       color: config.Colors().secondDarkColor(1),
                      //       height: 1.3),
                      //   headline3: TextStyle(
                      //       fontSize: 22.0,
                      //       fontWeight: FontWeight.w700,
                      //       color: config.Colors().secondDarkColor(1),
                      //       height: 1.3),
                      //   headline2: TextStyle(
                      //       fontSize: 24.0,
                      //       fontWeight: FontWeight.w700,
                      //       color: config.Colors().mainDarkColor(1),
                      //       height: 1.4),
                      //   headline1: TextStyle(
                      //       fontSize: 26.0,
                      //       fontWeight: FontWeight.w300,
                      //       color: config.Colors().secondDarkColor(1),
                      //       height: 1.4),
                      //   subtitle1: TextStyle(
                      //       fontSize: 18.0,
                      //       fontWeight: FontWeight.w500,
                      //       color: config.Colors().secondDarkColor(1),
                      //       height: 1.3),
                      //   headline6: TextStyle(
                      //       fontSize: 17.0,
                      //       fontWeight: FontWeight.w700,
                      //       color: config.Colors().mainDarkColor(1),
                      //       height: 1.3),
                      //   bodyText2: TextStyle(
                      //       fontSize: 14.0,
                      //       fontWeight: FontWeight.w400,
                      //       color: config.Colors().secondDarkColor(1),
                      //       height: 1.2),
                      //   bodyText1: TextStyle(
                      //       fontSize: 15.0,
                      //       fontWeight: FontWeight.w400,
                      //       color: config.Colors().secondDarkColor(1),
                      //       height: 1.3),
                      //   caption: TextStyle(
                      //       fontSize: 14.0,
                      //       fontWeight: FontWeight.w300,
                      //       color: config.Colors().secondDarkColor(0.6),
                      //       height: 1.2),
                      // ),
                    ));
        });
  }
}
