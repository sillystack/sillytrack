import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';

class SplashScreenController extends ControllerMVC {
  ValueNotifier<Map<String, double>> progress = new ValueNotifier(new Map());
  GlobalKey<ScaffoldState> scaffoldKey;

  SplashScreenController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    // Should define these variables before the app loaded
    progress.value = {"Setting": 0};
  }

  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 20), () {
      progress.value["Setting"] = 100;
      progress?.notifyListeners();
      scaffoldKey?.currentState?.showSnackBar(SnackBar(
        content: Text('Konek'),
      ));
    });
  }
}
