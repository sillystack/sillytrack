import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:flutter/material.dart';
import '../helpers/helper.dart';
import '../repository/waybill_repository.dart';
import '../models/waybill.dart';

class WaybillController extends ControllerMVC {
  Waybill waybill = new Waybill();
  OverlayEntry loader;
  GlobalKey<FormState> loginFormKey;
  GlobalKey<ScaffoldState> scaffoldKey;

  WaybillController() {
    loader = Helper.overlayLoader(context);
    loginFormKey = new GlobalKey<FormState>();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  Future<void> checkResi() async {
    FocusScope.of(context).unfocus();
    if (loginFormKey.currentState.validate()) {
      loginFormKey.currentState.save();
      Overlay.of(context).insert(loader);
      getResi(waybill).then((value) {
        setState(() {
          waybill = value;
        });
      }).catchError((e) {
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text(e),
        ));
      }).whenComplete(() {
        Helper.hideLoader(loader);
        loginFormKey.currentState.reset();
        scaffoldKey?.currentState?.showSnackBar(SnackBar(
          content: Text('Berhasil'),
        ));
      });
    }
  }

  Future<void> refreshPage() async {
    waybill = new Waybill();
  }
}
