import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import '../models/waybill.dart';

Future<Waybill> getResi(Waybill waybill) async {
  final String url = 'https://www.sillystack.com/tracking.php';
  final client = new http.Client();

  Map<String, String> body = {
    "no_resi": waybill.noresi,
    "expedisi": waybill.expedisi.toLowerCase()
  };

  print(body);
  final response = await client.post(url,
      headers: {
        HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded'
      },
      body: body);

  return Waybill.fromJSON(json.decode(response.body)['rajaongkir']['result']);
}
