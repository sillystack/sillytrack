import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/setting.dart';

ValueNotifier<Setting> setting = new ValueNotifier(new Setting());
final navigatorKey = GlobalKey<NavigatorState>();
//LocationData locationData;

Future<Setting> initSettings() async {
  Setting _setting;
  SharedPreferences prefs = await SharedPreferences.getInstance();

  _setting = Setting.fromJSON({'app_name': 'Sillytrack'});
  _setting.brightness.value =
      prefs.getBool('isDark') ?? false ? Brightness.dark : Brightness.light;
  setting.value = _setting;
  // ignore: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member
  setting.notifyListeners();
  return setting.value;
}

void setBrightness(Brightness brightness) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (brightness == Brightness.dark) {
    prefs.setBool("isDark", true);
    brightness = Brightness.dark;
  } else {
    prefs.setBool("isDark", false);
    brightness = Brightness.light;
  }
}
